var MapsGoogle = function () {
	var ggmap;
	var marker;

    var mapMarker = function (lat, lng) {
        var map = new GMaps({
            div: '#map',
            lat: lat,
            lng: lng,
			zoom: 18,
			disableDefaultUI:false,
        });
		
        marker = map.addMarker({
            lat: lat,
            lng: lng,
            //title: 'Auckland',
        });
		
		return map;
    }

    return {
        //main function to initiate route map
        init: function (lat, lng) { 
            ggmap = mapMarker(lat, lng);
        }
    };

}();