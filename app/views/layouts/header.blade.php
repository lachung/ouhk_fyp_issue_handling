<div class="page-header navbar navbar-fixed-top">
	<!-- BEGIN HEADER INNER -->
	<div class="page-header-inner">
		<!-- BEGIN LOGO -->
		<div class="page-logo">
			<a href="javascript:void(0)">
			<!--<img src="/assets/admin/layout/img/logo.png" alt="logo" class="logo-default"/> -->
			</a>
		</div>
		<!-- END LOGO -->
		<!-- BEGIN HORIZANTAL MENU -->
		<!-- DOC: Apply "hor-menu-light" class after the "hor-menu" class below to have a horizontal menu with white background -->
		<!-- DOC: This is desktop version of the horizontal menu. The mobile version is defined(duplicated) sidebar menu below. So the horizontal menu has 2 seperate versions -->
		<div class="hor-menu hor-menu-light hidden-sm hidden-xs">
			
			<ul class="nav navbar-nav">
				<!-- DOC: Remove data-hover="dropdown" and data-close-others="true" attributes below to disable the horizontal opening on mouse hover -->
				<?php if($page == 'index'){ ?>
				<li class="active">
				<?php }else{ ?>
				<li>
				<?php }?>
					<a href=" <?php echo URL::route('home.index.get') ?> ">
					Index 
					<?php if($page == 'index'){ ?>
						<span class="selected">
						</span>
					<?php }?>
					</a>
				</li>
				
				<?php if($page == 'report'){ ?>
				<li class="active">
				<?php }else{ ?>
				<li>
				<?php }?>
					<a href="<?php echo URL::route('report.index.get', ['page_num' => '1']) ?>">
					Issues 
					<?php if($page == 'report'){ ?>
						<span class="selected">
						</span>
					<?php }?>
					</a>
				</li>
				
				<?php if(@$_COOKIE['role'] == "1"){ ?>
					<?php if($page == 'manage'){ ?>
					<li class="classic-menu-dropdown active">
					<?php }else{ ?>
					<li class="classic-menu-dropdown">
					<?php }?>
						<a data-toggle="dropdown" href="javascript:;" data-hover="dropdown" data-close-others="true">
						Manage <i class="fa fa-angle-down"></i>
						</a>
						<ul class="dropdown-menu pull-left">
							<li>
								<a href="<?php echo URL::route('user.index.get', ['page_num' => '1']) ?>">
								<i class="fa fa-bookmark-o"></i> User Manage</a>
							</li>

					</li>
				<?php }?>
			</ul>
		</div>
		<!-- END HORIZANTAL MENU -->

		<!-- BEGIN RESPONSIVE MENU TOGGLER -->
		<a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
		</a>
		<!-- END RESPONSIVE MENU TOGGLER -->
		<!-- BEGIN TOP NAVIGATION MENU -->
		<div class="top-menu">
			<ul class="nav navbar-nav pull-right">
				<li class="dropdown dropdown-quick-sidebar-toggler" style="margin-right:20px">
					<a href="javascript:void(0)" class="dropdown-toggle">
						<i class="fa fa-user"><?=$_COOKIE['username']?></i>
					</a>
				</li>
				<!-- BEGIN QUICK SIDEBAR TOGGLER -->
				<li class="dropdown dropdown-quick-sidebar-toggler">
					<a href="<?php echo URL::route('user.logout.get')?>" class="dropdown-toggle">
						<i class="icon-logout"></i>
					</a>
				</li>
				<!-- END QUICK SIDEBAR TOGGLER -->
			</ul>
		</div>
		<!-- END TOP NAVIGATION MENU -->
	</div>
	<!-- END HEADER INNER -->
</div>
<!-- END HEADER -->