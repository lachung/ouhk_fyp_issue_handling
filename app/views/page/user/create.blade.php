@extends('layouts.default')

@section('default_style')
<?php include '../app/views/link/default.blade.php'; ?>
@stop

@section('custom_style')
@stop

@section('header')
<?php include '../app/views/layouts/header.blade.php'; ?>
@stop

@section('content')
<div class="col-md-offset-1 col-md-10 col-sm-offset-1 col-sm-10 blog-posts">
	<div class="row ">
		<h1>Manage - Create User</h1>
		<hr/>
	</div>

	<div class="row">
		<div class="portlet box green ">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-gift"></i> Data View
				</div>
			</div>
			<div class="portlet-body form">
				<form id="view_user_form" class="form-horizontal" role="form" enctype="multipart/form-data" method="post" action="">
					<div class="form-body">
						<h3>User's Information</h3>
						<hr/>
					
						<div class="form-group">
							<label class="col-md-3 control-label">User's Role</label>
							<div class="col-md-9">
								<select name="role" id="role" class="form-control input-medium">
									@foreach ($roles as $role)
										<option value="<?=$role->role_id?>"><?=$role->role_name?></option>
									@endforeach
								</select>
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-md-3 control-label">User's Department</label>
							<div class="col-md-9">
							
								<?php if($_COOKIE['department'] == '1'){?>
							
									<select name="department" id="department" class="form-control input-medium">
										@foreach ($departments as $department)
											<option value="<?=$department->department_id?>"><?=$department->department_name?></option>
										@endforeach
									</select>
								
								<?php }else{ ?>
									<input type="hidden" name="department" id="department" value="<?=$_COOKIE['department']?>">
									<select class="form-control input-medium" disabled>
										@foreach ($departments as $department)
											@if($department->department_id == $_COOKIE['department'])
												<option value="<?=$department->department_id?>" selected><?=$department->department_name?></option>
											@else
												<option value="<?=$department->department_id?>"><?=$department->department_name?></option>
											@endif
										@endforeach
									</select>
									
								<?php }?>
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-md-3 control-label">Username</label>
							<div class="col-md-9">
								<input id="username" name="username" type="text" class="form-control input-xlarge" placeholder="Username" >
								
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-md-3 control-label">Password</label>
							<div class="col-md-9">
								<input id="password" name="password" type="text" class="form-control input-xlarge" placeholder="Password" >
								
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-md-3 control-label">Position</label>
							<div class="col-md-9">
								<input id="position" name="position" type="text" class="form-control input-xlarge" placeholder="Position">
								
							</div>
						</div>
						

					</div>

					<div class="form-actions">
						<div class="row">
							<div class="col-md-offset-3 col-md-9">
								<button id="form_submit" type="submit" class="btn green">Submit</button>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12"></div>
	</div>

</div>

<div class="row">
	<div class="col-md-12"></div>
</div>
@stop

@section('default_script')
<?php include '../app/views/script/default.blade.php'; ?>
@stop

@section('custom_script')
<?php include '../app/views/script/user/view/custom.blade.php'; ?>
@stop


