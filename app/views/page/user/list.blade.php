@extends('layouts.default')

@section('default_style')
<?php include '../app/views/link/default.blade.php'; ?>
@stop

@section('custom_style')
<?php include '../app/views/link/default.blade.php'; ?>
@stop

@section('header')
<?php include '../app/views/layouts/header.blade.php'; ?>
@stop

@section('content')
<div class="col-md-offset-1 col-md-10 col-sm-offset-1 col-sm-10 blog-posts">
	<div class="row">
		<div class="col-md-12">
			<h1>Manage - User List</h1>
			<a  href="<?php echo URL::route('user.add.get')?>">Create a user</a>
		</div>
	</div>
	
	<hr/>
	
	<div class="row">
		<div class="table-responsive">
			<table class="table">
				<thead>
					<tr>
						<th>
							ID
						</th>
						<th>
							Username
						</th>
						<th>
							User's Department
						</th>
						<th>
							User's Role
						</th>
						<th>
							Create Time
						</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					@foreach($users as $user)
					<tr>
						<td>
							{{{$user->user_id}}}
						</td>
						<td>
							{{{$user->username}}}
						</td>
						<td>
							{{{$user->department_name}}}
						</td>
						<td>
							{{{$user->role_name}}}
						</td>
						<td>
							{{{$user->create_time}}}
						</td>
						<td><a href="<?php echo URL::route('user.view.get', ['user_id' => $user->user_id])?>">View</a></td>
						<td><a href="<?php echo URL::route('user.delete.get', ['user_id' => $user->user_id])?>">Delete</a></td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12"></div>
	</div>

	<div class="row">
		<div class="text-center">
			<ul class="pagination" style="position:relative;">
				@if($page_num == 1)
					<li class="active disabled">
					<a href="" disabled>
					<i class="fa fa-angle-left"></i>
					</a>
				@else
					<li>
					<a href="/manage/user/<?=$page_num-1?>">
					<i class="fa fa-angle-left"></i>
					</a>
				@endif
				</li>
				
				@for($i = 1; $i <= $num_of_page+1;$i++)
					@if($i == $page_num)
						<li class="active disabled">
					@else
						<li>
					@endif
					<a href="<?=$i?>">{{{$i}}} </a>
				</li>
				@endfor
				
				@if($page_num == $num_of_page+1)
					<li class="active disabled">
					<a href="" disabled>
					<i class="fa fa-angle-right"></i>
					</a>
				@else
					<li>
					<a href="/manage/user/<?=$num_of_page + 1?>">
					<i class="fa fa-angle-right"></i>
					</a>
				@endif
				</li>
			</ul>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
	</div>
</div>
@stop

@section('default_script')
<?php include '../app/views/script/default.blade.php'; ?>
@stop

@section('custom_script')
<?php include '../app/views/script/user/list/custom.blade.php'; ?>
@stop


