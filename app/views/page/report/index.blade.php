@extends('layouts.default')

@section('default_style')
<?php include '../app/views/link/default.blade.php'; ?>
@stop

@section('custom_style')
@stop

@section('header')
<?php include '../app/views/layouts/header.blade.php'; ?>
@stop

@section('content')
<div class="row">
	<div class="col-md-12">
		<h2>Issues Overview</h2>
		<hr/>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="table-responsive">
			<table class="table table-striped table-bordered table-hover">
				<thead>
					<tr>
						<th>#</th>
						<th>Issue</th>
						<th>Department</th>
						<th>In Charge</th>
						<th>Status</th>
						<th>Create Time</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					@foreach ($reports as $report)
					<tr>
						<td>{{{$report->report_id}}}</td>
						<td>{{{$report->description}}}</td>
						@if($report->department_name == 'N/A')
							<td>Not Assigned</td>
						@else
							<td>{{{$report->department_name}}}</td>
						@endif
						@if($report->username == 'N/A')
							<td>Not Assigned</td>
						@else
							<td>{{{$report->username}}}</td>
						@endif
						<td>
							@if ($report->status_id == 1)
								<span class="label label-sm label-danger">
							@elseif ($report->status_id == 2)
								<span class="label label-sm label-warning">
							@elseif ($report->status_id == 3)
								<span class="label label-sm label-info">
							@elseif ($report->status_id == 4)
								<span class="label label-sm label-info">
							@else
								<span class="label label-sm label-success">
							@endif
								{{{$report->status_name}}} 
							</span>
						</td>
						<td>{{{$report->create_time}}}</td>
						<td><a href="{{URL::route('report.detail.get', ['report_id' => $report->report_id])}}">Detail</a></td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>

<div class="row">
	<div class="text-center">
		<ul class="pagination" style="position:relative;">
			@if($page_num == 1)
				<li class="active disabled">
				<a href="/report/<?=$page_num?>">
			@else
				<li>
				<a href="/report/<?=$page_num-1?>">
			@endif
				<i class="fa fa-angle-left"></i>
				</a>
			</li>
			@for($i = 1; $i <= $num_of_page+1;$i++)
				@if($i == $page_num)
					<li class="active disabled">
				@else
					<li>
				@endif
				<a href="<?=$i?>">
				{{{$i}}} </a>
			</li>
			@endfor
			
			@if($page_num == $num_of_page + 1)
				<li class="active disabled">
				<a href="/report/<?=$page_num?>">
			@else
				<li>
				<a href="/report/<?=$page_num + 1?>">
			@endif
				<i class="fa fa-angle-right"></i>
				</a>
			</li>
		</ul>
	</div>
</div>
@stop

@section('default_script')
<?php include '../app/views/script/default.blade.php'; ?>
@stop

@section('custom_script')
<?php include '../app/views/script/index/custom.blade.php'; ?>
@stop


