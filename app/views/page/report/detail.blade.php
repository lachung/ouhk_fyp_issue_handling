@extends('layouts.default')

@section('default_style')
<?php include '../app/views/link/default.blade.php'; ?>
@stop

@section('custom_style')
@stop

@section('header')
<?php include '../app/views/layouts/header.blade.php'; ?>
@stop

@section('content')
<div class="row">
	<div class="col-md-12">
		<h2>Issue Detail</h2>
		<hr/>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="portlet box blue-hoki">
			<div class="portlet-title">
				<div class="caption">
					Issue Information
				</div>
			</div>
			
			<div class="portlet-body ">
				<div id="map" style="width: 100%; height: 350px">
				</div>
				<form action="#" class="form-horizontal form-bordered">
					<div class="form-body">
						
							<div class="form-group" >
								
							</div>
							
							<div class="form-group">
								<label class="control-label col-md-6">Issue ID</label>
								<div class="col-md-6">
									<label class="control-label col-md-6" style="text-align:left">{{{$report->report_id}}}</label>
								</div>
							</div>
							
							<div class="form-group">
								<label class="control-label col-md-6">Ref. ID</label>
								<div class="col-md-6">
									<label class="control-label col-md-6" style="text-align:left">{{{$report->ref_id}}}</label>
								</div>
							</div>
							
							<div class="form-group">
								<label class="control-label col-md-6">Department</label>
								<div class="col-md-6">
									<label class="control-label col-md-6" style="text-align:left">{{{$report->department_name}}}</label>
								</div>
							</div>
							
							<div class="form-group">
								<label class="control-label col-md-6">In Charge</label>
								<div class="col-md-6">
									<label class="control-label col-md-6" style="text-align:left">{{{$report->username}}}</label>
								</div>
							</div>
							
							<div class="form-group">
								<label class="control-label col-md-6">Status</label>
								<div class="col-md-6">
									<label class="control-label col-md-6" style="text-align:left">{{{$report->status_name}}}</label>
								</div>
							</div>
							
							<div class="form-group">
								<label class="control-label col-md-6">Title</label>
								<div class="col-md-6">
									<label class="control-label col-md-6" style="text-align:left">{{{$report->title}}}</label>
								</div>
							</div>
							
							<div class="form-group">
								<label class="control-label col-md-6">Description</label>
								<div class="col-md-6">
									<label class="control-label col-md-6" style="text-align:left">{{{$report->description}}}</label>
								</div>
							</div>
							
							<div class="form-group">
								<label class="control-label col-md-6">Location</label>
								<div class="col-md-6">
									<label class="control-label col-md-6" style="text-align:left">{{{$report->location}}}</label>
								</div>
							</div>
							
							<div class="form-group last">
								<label class="control-label col-md-6">Issue Received Time</label>
								<div class="col-md-6">
									<label class="control-label col-md-6" style="text-align:left">{{{$report->create_time}}}</label>
								</div>
							</div>
					</div>
				</form>
			</div>
		</div>
		
		<div class="portlet box blue-hoki">
			<div class="portlet-title">
				<div class="caption">
					Issue Photo
				</div>
			</div>
			
			<div class="portlet-body ">
				<form action="#" class="form-horizontal form-bordered">
					<div class="form-body">
						
							<div class="form-group" >
								<div class="col-md-offset-2 col-md-8">
									<img class="img-responsive" src="/s1108147/public/photo/<?=$report->report_id?>.<?=$report->img_type?>" ></img>
								</div>
							</div>

					</div>
				</form>
			</div>
		</div>
		
		<div class="portlet box blue-hoki">
			<div class="portlet-title">
				<div class="caption">
					Issue activities History
				</div>
			</div>
			
			<div class="portlet-body ">
				<div class="row">
					<div class="col-md-12">
						@if($histories == NULL)
							<div id="change_history_alert" class="alert alert-warning"> No activity in this issue. </div>
						@else
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover">
								<thead>
									<tr>
										<th>#</th>
										<th>From Department</th>
										<th> => </th>
										<th> To Department</th>
										<th>From User</th>
										<th> => </th>
										<th>To</th>
										<th>From Status</th>
										<th> => </th>
										<th>To Status</th>
										<th>Message</th>
										<th>Changed By</th>
										<th>Create Time</th>
									</tr>
								</thead>
								<tbody>
									<?php $i = count($histories);?>
									@foreach ($histories as $history)
									<tr>
										<td><?=$i?></td>
										
										@if($history->from_department_name == 'N/A')
											<td>Not Assigned</td>
										@else
											<td>{{{$history->from_department_name}}}</td>
										@endif
										<td> => </td>
										@if($history->to_department_name == 'N/A')
											<td>Not Assigned</td>
										@else
											<td>{{{$history->to_department_name}}}</td>
										@endif
										
										@if($history->from_username == 'N/A')
											<td>Not Assigned</td>
										@else
											<td>{{{$history->from_username}}}</td>
										@endif
										<td> => </td>
										@if($history->to_username == 'N/A')
											<td>Not Assigned</td>
										@else
											<td>{{{$history->to_username}}}</td>
										@endif
										
										<td>
											@if ($history->from_status == 1)
												<span class="label label-sm label-danger">
											@elseif ($history->from_status == 2)
												<span class="label label-sm label-warning">
											@elseif ($history->from_status == 3)
												<span class="label label-sm label-info">
											@elseif ($history->from_status == 4)
												<span class="label label-sm label-info">
											@else
												<span class="label label-sm label-success">
											@endif
												{{{$history->from_status_name}}} 
											</span>
										</td>
										<td> => </td>
										<td>
											@if ($history->to_status == 1)
												<span class="label label-sm label-danger">
											@elseif ($history->to_status == 2)
												<span class="label label-sm label-warning">
											@elseif ($history->to_status == 3)
												<span class="label label-sm label-info">
											@elseif ($history->to_status == 4)
												<span class="label label-sm label-info">
											@else
												<span class="label label-sm label-success">
											@endif
												{{{$history->to_status_name}}} 
											</span>
										</td>
										<td>{{{$history->message}}}</td>
										<td>{{{$history->username}}}</td>
										<td>{{{$history->create_time}}}</td>
									</tr>
									<?php $i--;?>
									@endforeach
								</tbody>
							</table>
						</div>
						@endif
					</div>
				</div>
			</div>
		</div>
		
		<div class="portlet box blue-steel">
			<div class="portlet-title">
				<div class="caption">
					Issue Status Change
				</div>
				
			</div>
			<div class="portlet-body form">
				<hr/>
				<div class="col-md-12">
					<div id="status_change_success_alert" class="alert alert-success"></div>
				</div>
			
				<form action="#" class="form-horizontal form-bordered">
					<div class="form-body">
						<div class="form-group">
							<label class="control-label col-md-3">Current Status</label>
							<div class="col-md-3">
								<select class="bs-select form-control" disabled>
									<option>{{{$report->status_name}}}</option>
								</select>
							</div>
						</div>
						
						<div class="form-group">
							<label class="control-label col-md-3">New Status</label>
							<div class="col-md-3">
								<select id="status_id" class="bs-select form-control">
									<option value="0"> === Select Status ===</option>
									@if($report->status_id == 1)
										<option value="2">Sorting</option>
									@elseif($report->status_id == 2)
										<option value="3">Sorted</option>
									@elseif($report->status_id == 3)
										<option value="4">Processing</option>
									@elseif($report->status_id == 4)
										<option value="2">Sorting</option>
										<option value="4">Processing</option>
										<option value="5">Completed</option>
									@else
									@endif
								</select>
							</div>
						</div>
						
						<div class="form-group last">
							<label class="control-label col-md-3">Comment</label>
							<div class="col-md-6">
								<input id="status_comment" type="text" class="form-control ">
							</div>
						</div>

						<div class="form-group last">
							<label class="control-label col-md-3"></label>
							<div class="col-md-3">
								<a id="status_submit" class="btn yellow">
								Submit <i class="fa fa-share"></i>
								</a>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
		
		<div class="portlet box blue-steel">
			<div class="portlet-title">
				<div class="caption">
					Issue Transfer
				</div>
				
			</div>
			<div class="portlet-body form">
				<hr/>
				<div class="col-md-12">
					<div id="report_transfer_success_alert" class="alert alert-success"></div>
				</div>
			
				<form action="#" class="form-horizontal form-bordered">
					<div class="form-body">
						<div class="form-group">
							<label class="control-label col-md-3">Current Department</label>
							<div class="col-md-3">
								<select class="bs-select form-control" disabled>
									<option>{{{$report->department_name}}}</option>
								</select>
							</div>
						</div>
						
						<div class="form-group">
							<label class="control-label col-md-3">New Department</label>
							<div class="col-md-3">
								<select id="department_id" class="bs-select form-control">
									<option value="0"> === Select Department ===</option>
									@foreach($departments as $department)
										<option value="<?=$department->department_id?>"><?=$department->department_name?></option>
									@endforeach
								</select>
							</div>
						</div>
						
						<div class="form-group last">
							<label class="control-label col-md-3">Comment</label>
							<div class="col-md-6">
								<input id="department_comment" type="text" class="form-control ">
							</div>
						</div>

						<div class="form-group last">
							<label class="control-label col-md-3"></label>
							<div class="col-md-3">
								<a id="transfer_submit" class="btn yellow">
								Submit <i class="fa fa-share"></i>
								</a>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
		
		<?php if($_COOKIE['role'] == "1"){?>
			<div class="portlet box blue-steel">
				<div class="portlet-title">
					<div class="caption">
						Issue Assign
					</div>
					
				</div>
				<div class="portlet-body form">
					<hr/>
					<div class="col-md-12">
						<div id="report_assign_success_alert" class="alert alert-success"></div>
					</div>
				
					<form action="#" class="form-horizontal form-bordered">
						<div class="form-body">
							<div class="form-group">
								<label class="control-label col-md-3">Current Assign To</label>
								<div class="col-md-3">
									<select class="bs-select form-control" disabled>
										<option>{{{$report->username}}}</option>
									</select>
								</div>
							</div>
							
							<div class="form-group">
								<label class="control-label col-md-3">New Assign To</label>
								<div class="col-md-3">
									<select id="user_id" class="bs-select form-control">
										<option value="0"> === Select User ===</option>
										@foreach($users as $user)
											<option value="<?=$user->user_id?>"><?=$user->username?></option>
										@endforeach
									</select>
								</div>
							</div>
							
							<div class="form-group last">
								<label class="control-label col-md-3">Comment</label>
								<div class="col-md-6">
									<input id="user_comment" type="text" class="form-control ">
								</div>
							</div>

							<div class="form-group last">
								<label class="control-label col-md-3"></label>
								<div class="col-md-3">
									<a id="assign_submit" class="btn yellow">
									Submit <i class="fa fa-share"></i>
									</a>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		<?php }?>
	</div>
</div>
<div class="page-footer-tools">
		<span class="go-top">
		<i class="fa fa-angle-up"></i>
		</span>
</div>
@stop

@section('default_script')
<?php include '../app/views/script/default.blade.php'; ?>
@stop

@section('custom_script')
<?php include '../app/views/script/report/detail/custom.blade.php'; ?>
@stop


