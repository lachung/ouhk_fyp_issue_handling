@extends('layouts.default')

@section('default_style')
<?php include '../app/views/link/default.blade.php'; ?>
@stop

@section('custom_style')
@stop

@section('header')
<?php include '../app/views/layouts/header.blade.php'; ?>
@stop

@section('content')
<div class="row">
	<div class="col-md-12">
		<h2>Add New Report</h2>
		<hr/>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="portlet box blue-hoki">
			<div class="portlet-title">
				<div class="caption">
					Report Information
				</div>
			</div>
			
			<div class="portlet-body ">
				<form action="<?php echo URL::route('report.submit.post')?>" method="post" enctype="multipart/form-data" class="form-horizontal form-bordered">
					<div class="form-body">
						
						<div class="form-group">
							<label class="control-label col-md-6">Title</label>
							<div class="col-md-6">
								<input type="text" name="title" class="form-control input-medium" placeholder="Title">
							</div>
						</div>
						
						<div class="form-group">
							<label class="control-label col-md-6">Description</label>
							<div class="col-md-6">
								<input type="text" name="description" class="form-control input-medium" placeholder="Description">
							</div>
						</div>
						
						<div class="form-group">
							<label class="control-label col-md-6">Location</label>
							<div class="col-md-6">
								<input type="text" name="location" class="form-control input-medium" placeholder="Location">
							</div>
						</div>
						
						<div class="form-group">
							<label class="control-label col-md-6">Latitude</label>
							<div class="col-md-6">
								<input type="text" name="latitude" class="form-control input-medium" placeholder="Latitude">
							</div>
						</div>
						
						<div class="form-group">
							<label class="control-label col-md-6">Longitude</label>
							<div class="col-md-6">
								<input type="text" name="longitude" class="form-control input-medium" placeholder="Longitude">
							</div>
						</div>
						
						<div class="form-group last">
							<label class="control-label col-md-6">Image</label>
							<div class="col-md-6">
								<input type="file" name="image_file" id="image_file" accept="image/*">
							</div>
						</div>
						
						<div class="form-actions">
							<div class="row">
								<div class="col-md-offset-6 col-md-6">
									<button type="submit" class="btn green">Submit</button>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<div class="page-footer-tools">
		<span class="go-top">
		<i class="fa fa-angle-up"></i>
		</span>
</div>
@stop

@section('default_script')
<?php include '../app/views/script/default.blade.php'; ?>
@stop

@section('custom_script')
<?php include '../app/views/script/demo/index/custom.blade.php'; ?>
@stop


