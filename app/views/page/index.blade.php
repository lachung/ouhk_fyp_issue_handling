@extends('layouts.default')

@section('default_style')
<?php include '../app/views/link/default.blade.php'; ?>
@stop

@section('custom_style')
@stop

@section('header')
<?php include '../app/views/layouts/header.blade.php'; ?>
@stop

@section('content')
<div class="row">
	<div class="col-md-offset-1 col-md-10">
		<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
			<div class="dashboard-stat blue-madison">
				<div class="visual">
					<i class="fa fa-comments"></i>
				</div>
				<div class="details">
					<div class="number">
						{{{$total_report_count}}}
					</div>
					<div class="desc">
						 Total Issues
					</div>
				</div>
				<a class="more" href="#">
				View more <i class="m-icon-swapright m-icon-white"></i>
				</a>
			</div>
		</div>
		<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
			<div class="dashboard-stat red-intense">
				<div class="visual">
					<i class="fa fa-bar-chart-o"></i>
				</div>
				<div class="details">
					<div class="number">
						 {{{$processing_report_count}}}
					</div>
					<div class="desc">
						 Processing Issues
					</div>
				</div>
				<a class="more" href="#">
				View more <i class="m-icon-swapright m-icon-white"></i>
				</a>
			</div>
		</div>
		<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
			<div class="dashboard-stat green-haze">
				<div class="visual">
					<i class="fa fa-shopping-cart"></i>
				</div>
				<div class="details">
					<div class="number">
						 {{{$completed_report_count}}}
					</div>
					<div class="desc">
						 Completed Issues
					</div>
				</div>
				<a class="more" href="#">
				View more <i class="m-icon-swapright m-icon-white"></i>
				</a>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-offset-1 col-md-10">
		<div class="portlet box blue-steel">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-bell-o"></i>Today Received
				</div>
				
			</div>
			<div class="portlet-body">
				<div id="slimScrollDiv_1" class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: auto;"><div class="scroller" style="overflow: hidden; width: auto; height: auto;" data-always-visible="1" data-rail-visible="0" data-initialized="1">
					<ul class="feeds">
						@forelse($reports as $report)
						<li>
							<div class="col1">
								<div class="cont">
									<div class="cont-col1">
										<div class="label label-sm label-info">
											<i class="fa fa-check"></i>
										</div>
									</div>
									<div class="cont-col2">
										<div class="desc">
											 Report received. Ref: {{$report->ref_id}}
											
										</div>
									</div>
								</div>
							</div>
							<div class="col2">
								<div class="date">

								</div>
							</div>
						</li>
						
						@empty
							<div class="alert alert-warning">No issues received today.</div>
						@endforelse
						
					</ul>
				</div><div class="slimScrollBar" style="width: 7px; position: absolute; opacity: 0.4; border-top-left-radius: 7px; border-top-right-radius: 7px; border-bottom-right-radius: 7px; border-bottom-left-radius: 7px; z-index: 99; right: 1px; top: 30px; height: 191.897654584222px; display: block; background: rgb(187, 187, 187);"></div><div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-top-left-radius: 7px; border-top-right-radius: 7px; border-bottom-right-radius: 7px; border-bottom-left-radius: 7px; opacity: 0.2; z-index: 90; right: 1px; background: rgb(234, 234, 234);"></div></div>
				<div class="scroller-footer">
					<div class="btn-arrow-link pull-right">
						<a href="report/1">See All Issues</a>
						<i class="icon-arrow-right"></i>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-offset-1 col-md-10">
		<div class="portlet box blue-steel">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-bell-o"></i>Today Activities
				</div>
				
			</div>
			<div class="portlet-body">
				<div id="slimScrollDiv_2" class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: auto;"><div class="scroller" style="overflow: hidden; width: auto; height: auto;" data-always-visible="1" data-rail-visible="0" data-initialized="1">
					<ul class="feeds">
						@forelse($histories as $history)
						<li>
							<div class="col1">
								<div class="cont">
									<div class="cont-col1">
										<div class="label label-sm label-info">
											<i class="fa fa-check"></i>
										</div>
									</div>
									<div class="cont-col2">
										<div class="desc">
											@if($history->from_department_name != $history->to_department_name)
												Report transferred by {{$history->by_username}}. [{{$history->from_department_name}} -> {{$history->to_department_name}}] Ref: {{$history->ref_id}}
											@endif
											
											@if($history->from_status_name != $history->to_status_name)
												Report status changed by {{$history->by_username}}. [{{$history->from_status_name}} -> {{$history->to_status_name}}]  Ref: {{$history->ref_id}}
											@endif
											
											@if($history->from_username != $history->to_username)
												Report assigned to [{{$history->to_username}} by {{$history->by_username}}]  Ref: {{$history->ref_id}}
											@endif
										</div>
									</div>
								</div>
							</div>
							<div class="col2">
								<div class="date">
									
								</div>
							</div>
						</li>
						
						@empty
							<div class="alert alert-warning">No activities today.</div>
						@endforelse
						
					</ul>
				</div><div class="slimScrollBar" style="width: 7px; position: absolute; opacity: 0.4; border-top-left-radius: 7px; border-top-right-radius: 7px; border-bottom-right-radius: 7px; border-bottom-left-radius: 7px; z-index: 99; right: 1px; top: 30px; height: 191.897654584222px; display: block; background: rgb(187, 187, 187);"></div><div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-top-left-radius: 7px; border-top-right-radius: 7px; border-bottom-right-radius: 7px; border-bottom-left-radius: 7px; opacity: 0.2; z-index: 90; right: 1px; background: rgb(234, 234, 234);"></div></div>
				<div class="scroller-footer">

				</div>
			</div>
		</div>
	</div>
</div>

@stop

@section('footer')
<?php include '../app/views/layouts/footer.blade.php'; ?>
@stop

@section('default_script')
<?php include '../app/views/script/default.blade.php'; ?>
@stop

@section('custom_script')
<?php include '../app/views/script/index/custom.blade.php'; ?>
@stop

