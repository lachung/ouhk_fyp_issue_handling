<script>
jQuery(document).ready(function() {    
   Metronic.init(); // init metronic core components
	Layout.init(); // init current layout
	QuickSidebar.init() // init quick sidebar
	
	$('#slimScrollDiv_1').slimScroll({
		height: '300px'
	});
	$('#slimScrollDiv_2').slimScroll({
		height: '300px'
	});

});
</script>