<script src="http://maps.google.com/maps/api/js?sensor=true" type="text/javascript"></script>
<script src="/s1108147/public/assets/global/plugins/gmaps/gmaps.js" type="text/javascript"></script>
<script src="/s1108147/public/assets/custom/js/custom_map.js" type="text/javascript"></script>

<script>
	jQuery(document).ready(function() {
		Metronic.init();
		Layout.init();
		MapsGoogle.init(<?=$report->latitude?>, <?=$report->longitude?>);
		
		$('#status_change_success_alert').hide();
		$('#report_transfer_success_alert').hide();
		$('#report_assign_success_alert').hide();
		
		$('#status_submit').live('click', function(){
			$.post("<?php echo URL::route('history.submit.post')?>",
            {
				type: 0,
                report_id: <?=$report->report_id?>,
                user_id: <?=$_COOKIE['user_id']?>,
                from_department: <?=$report->department_id?>,
                to_department: <?=$report->department_id?>,
                from_status: <?=$report->status_id?>,
                to_status: $('#status_id').val(),
                from_user: <?=$report->user_id?>,
                to_user: <?=$report->user_id?>,
                message: $('#status_comment').val()
            })
            .done(function(data){
                $('#status_change_success_alert').empty()
                $('#status_change_success_alert').append("Status Changed.");
                $('#status_change_success_alert').show();
            });
		});
		
		$('#transfer_submit').live('click', function(){
			$.post("<?php echo URL::route('history.submit.post')?>",
            {
				type: 1,
                report_id: <?=$report->report_id?>,
                user_id: <?=$_COOKIE['user_id']?>,
                from_department: <?=$report->department_id?>,
                to_department: $('#department_id').val(),
                from_status: <?=$report->status_id?>,
                to_status: <?=$report->status_id?>,
                from_user: <?=$report->user_id?>,
                to_user: <?=$report->user_id?>,
                message: $('#department_comment').val()
            })
            .done(function(data){
                $('#report_transfer_success_alert').empty()
                $('#report_transfer_success_alert').append("Report Transferred.");
                $('#report_transfer_success_alert').show();
            });
		});
		
		$('#assign_submit').live('click', function(){
			$.post("<?php echo URL::route('history.submit.post')?>",
            {
				type: 2,
                report_id: <?=$report->report_id?>,
                user_id: <?=$_COOKIE['user_id']?>,
                from_department: <?=$report->department_id?>,
                to_department: <?=$report->department_id?>,
                from_status: <?=$report->status_id?>,
                to_status: <?=$report->status_id?>,
                from_user: <?=$report->user_id?>,
                to_user: $('#user_id').val(),
                message: $('#user_comment').val()
            })
            .done(function(data){
                $('#report_assign_success_alert').empty()
                $('#report_assign_success_alert').append("Report Assigned");
                $('#report_assign_success_alert').show();
            });
		});
	});
</script>