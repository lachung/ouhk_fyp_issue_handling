<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="/s1108147/public/assets/global/plugins/respond.min.js"></script>
<script src="/s1108147/public/assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
<script src="/s1108147/public/assets/global/plugins/jquery-1.11.0.min.js" type="text/javascript"></script>
<script src="/s1108147/public/assets/global/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="/s1108147/public/assets/global/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
<script src="/s1108147/public/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="/s1108147/public/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="/s1108147/public/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="/s1108147/public/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="/s1108147/public/assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="/s1108147/public/assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="/s1108147/public/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<script src="/s1108147/public/assets/global/plugins/rateit/src/jquery.rateit.js" type="text/javascript"></script>
<script src="/s1108147/public/assets/frontend/layout/scripts/back-to-top.js" type="text/javascript"></script>  
<!-- END CORE PLUGINS -->
<script src="/s1108147/public/assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="/s1108147/public/assets/admin/layout/scripts/layout.js" type="text/javascript"></script>
<script src="/s1108147/public/assets/admin/layout/scripts/quick-sidebar.js" type="text/javascript"></script>

<!-- END JAVASCRIPTS-->