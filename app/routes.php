<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/


Route::get('/login', ['as' => 'user.login.get', 'uses' => 'UserController@login']);
Route::post('/login', ['as' => 'user.login.post', 'uses' => 'UserController@loginCheck']);
Route::get('/logout',  ['as' => 'user.logout.get', 'uses' => 'UserController@logout']);

Route::group(array('before' => 'login_auth_check'), function(){
	Route::get('/', ['as' => 'home.index.get', 'uses' => 'HomeController@index']);
	Route::get('/report/{page_num}', ['as' => 'report.index.get', 'uses' => 'ReportController@index']);
	Route::get('/report/detail/{report_id}', ['as' => 'report.detail.get', 'uses' => 'ReportController@detail']);
	Route::post('/history/submit', ['as' => 'history.submit.post', 'uses' => 'HistoryController@submit']);
	
	
});

/*
	Manage Features Routes
*/

Route::group(array('before' => 'admin_auth_check'), function(){
	Route::get('/manage/user/add', ['as' => 'user.add.get', 'uses' => 'UserController@userCreatePage']);
	Route::post('/manage/user/add', ['as' => 'user.add.post', 'uses' => 'UserController@userCreateAction']);
	Route::get('/manage/user/{page_num}', ['as' => 'user.index.get', 'uses' => 'UserController@userList']);
	Route::get('/manage/user/view/{user_id}', ['as' => 'user.view.get', 'uses' => 'UserController@userViewPage']);
	Route::post('/manage/user/view/{user_id}', ['as' => 'user.view.post', 'uses' => 'UserController@userEditAction']);
	Route::get('/manage/user/delete/{user_id}', ['as' => 'user.delete.get', 'uses' => 'UserController@userDeleteAction']);
});



/*
	API Routes
	
*/
/*
	Action "submit" parameters required:
	- title : String (title of the report)
	- description : String (description of the report)
	- location : String (location in text)
	- latitude : decimal (coordination)
	- longitude : decimal (coordination)
	- image_file : File (image/*)
	
	Action "submit" return:
		SUCCESS:
		- An reference ID with 13 digits 
			
		FAIL:
		- An error code
	
	Error code :
	- "-1", Image file no found
	- "-2", Can not insert the record, may be system error
*/
Route::post('/api/report/submit', ['as' => 'report.submit.post', 'uses' => 'APIController@submit']);


/*
	Action "getStatusByID" parameters required:
	- Reference ID with 13 digits
	
	Action "getStatusByID" return:
		SUCCESS:
		- An Status in text
		
		FAIL:
		- 0
*/
Route::any('/api/report/status/{id}', 'APIController@getStatusByID');

/*
	Action "getHistoryByID" parameters required:
	- Reference ID with 13 digits
	
	Action "getHistoryByID" return:
		SUCCESS:
		- Object array encoded by json
		
		FAIL:
		- 0
*/
Route::any('/api/report/history/{id}', 'APIController@getHistoryByID');

/*
	Action "getDepartmentList" parameters required:
	- NO
	
	Action "getDepartmentList" return:
	- Object array encoded by json
*/
Route::any('/api/department/list', 'APIController@getDepartmentList');

/*
	Action "getStatusList" parameters required:
	- NO
	
	Action "getStatusList" return:
	- Object array encoded by json
*/
Route::any('/api/status/list', 'APIController@getStatusList');

/*
	Demo for API
*/
Route::get('/api/report/add', ['as' => 'report.add.get', 'uses' => 'APIController@demo']);
Route::any('/api/ref_id/test', 'APIController@getNewRefId');