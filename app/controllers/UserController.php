<?php

class UserController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function login(){
		if(isset($_COOKIE['user_id'])){
			return Redirect::to('/');
		}else{
			return View::make('page/user/login');
		}
	}
	
	public function logout(){
		$this->resetCookie();
		return Redirect::to('/');
	}
	
	public function loginCheck(){
		$username = Input::get('username');
		$password = md5(Input::get('password'));
		
		$user = DB::table('user')
					->where('username', $username)
					->where('password', $password)
					->where('is_deleted', 0)
					->first();
		//return var_dump($user);
		
		if($user != NULL){
			$this->resetCookie();
			setcookie("user_id", $user->user_id, 0, "/s1108147");
			setcookie("username", $user->username, 0, "/s1108147");
			setcookie("role", $user->role_id, 0, "/s1108147");
			setcookie("department", $user->department_id, 0, "/s1108147");
			return Redirect::to('/report/1');
		}else{
			return Redirect::to('/login');
		}
	}
	
	private function resetCookie(){
		setcookie("user_id", "", -1, "/s1108147");
		setcookie("username", "", -1, "/s1108147");
		setcookie("role", "", -1, "/s1108147");
		setcookie("department", "", -1, "/s1108147");
	}
	
	/* 
	 * ================Start of user management feather & page===================
	 */
	
	public function userList($page_num){
		$temp = 0;
		if(is_numeric($page_num)){
			$temp = ($page_num - 1) * 20;
			if($temp < 0){
				$temp = 0;
				$page_num = 1;
			}
		}else{
			$page_num = 1;
		}
		
		if($_COOKIE['department'] == "1"){
			$users = DB::table('user')
					->where('is_deleted', 0)
					->leftjoin('role', 'user.role_id', '=', 'role.role_id')
					->leftjoin('department', 'user.department_id', '=', 'department.department_id')
					->select('user.user_id', 'user.username', 'user.create_time', 'role.role_name', 'department.department_name')
					->orderBy('user.create_time', 'desc')
					->skip($temp)->take(20)->get();
			
			$obj_count = DB::table('user')->count();
			
		}else{
						
			$users = DB::table('user')
					->where('user.department_id', $_COOKIE['department'])
					->where('is_deleted', 0)
					->leftjoin('role', 'user.role_id', '=', 'role.role_id')
					->leftjoin('department', 'user.department_id', '=', 'department.department_id')
					->select('user.user_id', 'user.username', 'user.create_time', 'role.role_name', 'department.department_name')
					->orderBy('user.create_time', 'desc')
					->skip($temp)->take(20)->get();
						
			$obj_count = DB::table('user')
						->where('user.department_id', $_COOKIE['department'])
						->count();
		}
		
		
		$num_of_page = round(($obj_count / 20), 0, PHP_ROUND_HALF_UP);

		return View::make('page/user/list', array('page' => 'manage',
													'users' => $users,
													'num_of_page' => $num_of_page,
													'page_num' => $page_num));
	}
	
	public function userViewPage($user_id){
		$user = DB::table('user')
						->where('user.user_id', '=', $user_id)
						->where('is_deleted', 0)
						->leftjoin('role', 'user.role_id', '=', 'role.role_id')
						->leftjoin('department', 'user.department_id', '=', 'department.department_id')
						->select('user.user_id', 'user.username', 'user.position', 'user.create_time', 'user.role_id', 'user.department_id', 'role.role_name', 'department.department_name')
						->first();
						
		$roles = DB::table('role')
						->select('role_id', 'role_name')
						->get();
		
		$departments = DB::table('department')
						->select('department_id', 'department_name')
						->get();
		
		return View::make('page/user/view', array('page' => 'manage',
															'user' => $user,
															'roles' => $roles,
															'departments' => $departments));
	}
	
	public function userCreatePage(){
		$roles = DB::table('role')
						->select('role_id', 'role_name')
						->get();
		
		$departments = DB::table('department')
						->select('department_id', 'department_name')
						->get();
		
		return View::make('page/user/create', array('page' => 'manage',
													'roles' => $roles,
													'departments' => $departments));
	}
	
	public function userCreateAction(){
		$role = Input::get('role');
		$department = Input::get('department');
		$username = Input::get('username');
		$password = Input::get('password');
		$position = Input::get('position');
		
		DB::table('user')
				->insert(array('role_id' => $role,
								'department_id' => $department,
								'username' => $username,
								'password' => md5($password),
								'position' => $position,
								'create_time' => date('Y-m-d H:i:s'))
		);
		return Redirect::action('UserController@userList');
	}
	
	public function userEditAction($user_id){
		$role = Input::get('role');
		$department = Input::get('department');
		
		DB::table('user')
				->where('user_id', $user_id)
				->update(array('role_id' => $role,
								'department_id' => $department)
		);
		return Redirect::action('UserController@userList');
	}
	
	public function userDeleteAction($user_id){
		
		DB::table('user')
				->where('user_id', $user_id)
				->update(array('is_deleted' => 1)
		);
		return Redirect::action('UserController@userList');
	}
	/* 
	 * ================End of user management feather & page===================
	 */ 
}
