<?php

class ReportController extends BaseController {
	public function index($page_num){
		$temp = 0;
		if(is_numeric($page_num)){
			$temp = ($page_num - 1) * 20;
			if($temp < 0){
				$temp = 0;
				$page_num = 1;
			}
		}else{
			$page_num = 1;
		}
		
		/*
		$sql = 'SELECT report.*, user.username, status.status_name, department.department_name FROM report ';
		$sql .= 'LEFT JOIN department ON report.department_id=department.department_id ';
		$sql .= 'LEFT JOIN user ON report.user_id=user.user_id ';
		$sql .= 'LEFT JOIN status ON report.status_id=status.status_id ';
		$sql .= 'ORDER BY report.create_time DESC LIMIT ' . $temp . ', 10';
		$reports = DB::Select($sql);
		*/
		
		if($_COOKIE['role'] == "1"){
			if($_COOKIE['department'] == "1"){
				$reports = DB::table('report')
								->leftjoin('department', 'report.department_id', '=', 'department.department_id')
								->leftjoin('user', 'report.user_id', '=', 'user.user_id')
								->leftjoin('status', 'report.status_id', '=', 'status.status_id')
								->select('report.*', 'user.username', 'status.status_name', 'department.department_name')
								->orderBy('report.create_time', 'desc')
								->skip($temp)->take(20)->get();
				
				$obj_count = DB::table('report')->count();
				
			}else{
				$reports = DB::table('report')
							->where('report.department_id', $_COOKIE['department'])
							->leftjoin('department', 'report.department_id', '=', 'department.department_id')
							->leftjoin('user', 'report.user_id', '=', 'user.user_id')
							->leftjoin('status', 'report.status_id', '=', 'status.status_id')
							->select('report.*', 'user.username', 'status.status_name', 'department.department_name')
							->orderBy('report.create_time', 'desc')
							->skip($temp)->take(20)->get();
							
				$obj_count = DB::table('report')
							->where('report.department_id', $_COOKIE['department'])
							->count();
			}
		}else{
			$reports = DB::table('report')
							->where('report.user_id', $_COOKIE['user_id'])
							->leftjoin('department', 'report.department_id', '=', 'department.department_id')
							->leftjoin('user', 'report.user_id', '=', 'user.user_id')
							->leftjoin('status', 'report.status_id', '=', 'status.status_id')
							->select('report.*', 'user.username', 'status.status_name', 'department.department_name')
							->orderBy('report.create_time', 'desc')
							->skip($temp)->take(20)->get();
							
			$obj_count = DB::table('report')
							->where('report.user_id', $_COOKIE['user_id'])
							->count();
		}
		
		
		$num_of_page = round(($obj_count / 20), 0, PHP_ROUND_HALF_UP);
		//echo $obj_count;

		return View::make('page/report/index', array('page' => 'report',
													'reports' => $reports,
													'num_of_page' => $num_of_page,
													'page_num' => $page_num));
	}
	
	public function detail($report_id){
		$report = DB::table('report')
						->where('report_id', $report_id)
						->leftjoin('department', 'report.department_id', '=', 'department.department_id')
						->leftjoin('user', 'report.user_id', '=', 'user.user_id')
						->leftjoin('status', 'report.status_id', '=', 'status.status_id')
						->select('report.*', 'department.department_name', 'user.username', 'status.status_name')
						->first();
						
		$histories = DB::table('history')
						->where('report_id', $report_id)
						->leftjoin('department AS from_department', 'history.from_department', '=', 'from_department.department_id')
						->leftjoin('department AS to_department', 'history.to_department', '=', 'to_department.department_id')
						->leftjoin('status AS from_status', 'history.from_status', '=', 'from_status.status_id')
						->leftjoin('status AS to_status', 'history.to_status', '=', 'to_status.status_id')
						->leftjoin('user', 'history.user_id', '=', 'user.user_id')
						->leftjoin('user AS from_user', 'history.from_user', '=', 'from_user.user_id')
						->leftjoin('user AS to_user', 'history.to_user', '=', 'to_user.user_id')
						->select('history.*',
								'user.username',
								'from_department.department_name AS from_department_name',
								'to_department.department_name AS to_department_name',
								'from_status.status_name AS from_status_name',
								'to_status.status_name AS to_status_name',
								'from_user.username AS from_username',
								'to_user.username AS to_username')
						->orderBy('create_time', 'desc')
						->get();
						
		$sql = 'SELECT * FROM department LIMIT 1, 10';
		$departments = DB::Select($sql);
		
		$sql = 'SELECT user.username, user.user_id FROM user WHERE user.department_id = ' . $report->department_id;
		$users = DB::Select($sql);
		
		if($report != NULL){
			return View::make('page/report/detail', array('page' => 'report',
														'report' => $report,
														'histories' => $histories,
														'departments' => $departments,
														'users' => $users));
		}else{
			return Redirect::to('/report/1');
		}
	}
}
