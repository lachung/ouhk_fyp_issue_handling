<?php

class HistoryController extends BaseController {
	public function submit(){
		$type = Input::get('type');
		$report_id = Input::get('report_id');
		$user_id = Input::get('user_id');
		$from_department = Input::get('from_department');
		$to_department = Input::get('to_department');
		$from_status = Input::get('from_status');
		$to_status = Input::get('to_status');
		$from_user = Input::get('from_user');
		$to_user = Input::get('to_user');
		$message = Input::get('message');
		
		$report = DB::table('report')
						->where('report_id', $report_id)
						->first();
		
		if($type == "0"){
			$from_department = $report->department_id;
			$to_department = $report->department_id;
			$from_user = $report->user_id;
			$to_user = $report->user_id;
			$from_status = $report->status_id;
		
			DB::table('report')
				->where('report_id', $report_id)
				->update(array('status_id' => $to_status));
				
		}elseif($type == "1"){
			$from_department = $report->department_id;
			$from_user = $report->user_id;
			$to_user = $report->user_id;
			$from_status = $report->status_id;
			$to_status = $report->status_id;
			
			DB::table('report')
				->where('report_id', $report_id)
				->update(array('department_id' => $to_department));
				
		}elseif($type == "2"){
			$from_department = $report->department_id;
			$to_department = $report->department_id;
			$from_user = $report->user_id;
			$from_status = $report->status_id;
			$to_status = $report->status_id;
		
			DB::table('report')
				->where('report_id', $report_id)
				->update(array('user_id' => $to_user));
		}
		
		$id = DB::table('history')->insertGetId(
			array('report_id' => $report_id,
				'user_id' => $user_id,
				'from_department' => $from_department,
				'to_department' => $to_department,
				'from_status' => $from_status,
				'to_status' => $to_status,
				'from_user' => $from_user,
				'to_user' => $to_user,
				'message' => $message,
				'create_time' => date('Y-m-d H:i:s'))
		);
		
		if($id > 0){
			return "1";
		}else{
			return "0";
		}
	}
}
