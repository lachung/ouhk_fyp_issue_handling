<?php

class APIController extends BaseController {
	public function submit(){
		$photo_path = getcwd() . '/photo/';
		$title = Input::get('title');
		$description = Input::get('description');
		$location = Input::get('location');
		$latitude = Input::get('latitude');
		$longitude = Input::get('longitude');


		if(Input::hasFile('image_file')){
			$extension = Input::file('image_file')->getClientOriginalExtension();

			while(true){
				$rand_num = mt_rand(0, 99999);

				$ref_num = $date . sprintf("%'.05d", $rand_num);

				$count = DB::table('report')
							->where('ref_id', '=', $ref_num)
							->count();
				if($count == 0){
					break;
				}
			}

			$id = DB::table('report')->insertGetId(
				array('title' => $title,
					'description' => $description,
					'location' => $location,
					'latitude' => $latitude,
					'longitude' => $longitude,
					'img_type' => $extension,
					'ref' => $ref_id,
					'create_time' => date('Y-m-d H:i:s'))
			);
			if($id > 0){
				$extension = Input::file('image_file')->getClientOriginalExtension();
				Input::file('image_file')->move($photo_path, $id . "." . $extension);
			}else{
				return -2;
			}
		}else{
			return -1;
		}
		//return $id;
		return $ref_num;
	}

	public function getStatusByID($id){
		if(is_numeric($id)){
			$report = DB::table('report')
						->where('ref_id', $id)
						->leftjoin('status', 'report.status_id', '=', 'status.status_id')
						->first();
			if($report != null){
				return $report->status_name;
			}

		}
		return 0;
	}

	public function getHistoryByID($id){
		if(is_numeric($id)){
			$report = DB::table('report')->where('ref_id', '=', $id)->first();

			if($report != null){
				$report_id = $report->report_id;

				$histories = DB::table('history')->where('report_id', $report_id)
							->leftjoin('department AS from_department', 'history.from_department', '=', 'from_department.department_id')
							->leftjoin('department AS to_department', 'history.to_department', '=', 'to_department.department_id')
							->leftjoin('status AS from_status', 'history.from_status', '=', 'from_status.status_id')
							->leftjoin('status AS to_status', 'history.to_status', '=', 'to_status.status_id')
							->leftjoin('user', 'history.user_id', '=', 'user.user_id')
							->leftjoin('user AS from_user', 'history.from_user', '=', 'from_user.user_id')
							->leftjoin('user AS to_user', 'history.to_user', '=', 'to_user.user_id')
							->select('history.create_time',
									'user.username AS by_username',
									'from_department.department_name AS from_department_name',
									'to_department.department_name AS to_department_name',
									'from_status.status_name AS from_status_name',
									'to_status.status_name AS to_status_name',
									'from_user.username AS from_username',
									'to_user.username AS to_username')
							->orderBy('history.create_time', 'desc')
							->get();
				return json_encode($histories);
			}
			return -1;
		}
		return 0;
	}

	public function getDepartmentList(){
		$departments = DB::table('department')
							->select('department_id', 'department_name')
							->get();
		return json_encode($departments);
	}

	public function getStatusList(){
		$status = DB::table('status')
							->select('status_id', 'status_name')
							->get();
		return json_encode($status);
	}

	public function getNewRefId(){
		$date = date('Ymd');
		$rand_num = mt_rand(0, 99999);
		$ref_num = $date . sprintf("%'.05d", $rand_num);
		return $ref_num;
	}

	public function demo(){
		return View::make('page/demo/index', array('page' => 'report'));
	}
}
