<?php

class HomeController extends BaseController {
	
	const STATE_SUBMITTED = 1;
	const STATE_SORTING = 2;
	const STATE_SORTED = 3;
	const STATE_PROCESSING = 4;
	const STATE_COMPLETED = 5;

	public function index(){
		$total_report_count = 0;
		$processing_report_count = 0;
		$completed_report_count = 0;
		
		if($_COOKIE['role'] == "1"){
			if($_COOKIE['department'] == "1"){
				$total_report_count = DB::table('report')->count();
				$processing_report_count = DB::table('report')
												->whereIn('report.status_id', array(HomeController::STATE_SORTING, HomeController::STATE_SORTED, HomeController::STATE_PROCESSING))
												->count();
												
				$completed_report_count = DB::table('report')
											->where('report.status_id', '=', HomeController::STATE_COMPLETED)
											->count();
											
				$reports = DB::table('report')
								->where('create_time', '>', date('Y-m-d 0:0:0'))
								->skip(0)->take(15)
								->get();
								
				$histories = DB::table('history')
								->leftjoin('report', 'history.report_id', '=', 'report.report_id')
								->leftjoin('department AS from_department', 'history.from_department', '=', 'from_department.department_id')
								->leftjoin('department AS to_department', 'history.to_department', '=', 'to_department.department_id')
								->leftjoin('status AS from_status', 'history.from_status', '=', 'from_status.status_id')
								->leftjoin('status AS to_status', 'history.to_status', '=', 'to_status.status_id')
								->leftjoin('user', 'history.user_id', '=', 'user.user_id')
								->leftjoin('user AS from_user', 'history.from_user', '=', 'from_user.user_id')
								->leftjoin('user AS to_user', 'history.to_user', '=', 'to_user.user_id')
								->where('history.create_time', '>', date('Y-m-d 0:0:0'))
								->select('report.ref_id', 'from_department.department_name AS from_department_name', 
															'to_department.department_name AS to_department_name', 
															'from_status.status_name AS from_status_name', 
															'to_status.status_name AS to_status_name', 
															'from_user.username AS from_username',
															'to_user.username AS to_username',
															'user.username AS by_username')
								->orderBy('history.create_time', 'desc')
								->skip(0)->take(15)
								->get();
			}else{
				$total_report_count = DB::table('report')
											->where('report.department_id', '=', $_COOKIE['department'])
											->count();
											
				$processing_report_count = DB::table('report')
												->whereIn('report.status_id', array(HomeController::STATE_SORTING, HomeController::STATE_SORTED, HomeController::STATE_PROCESSING))
												->where('report.department_id', '=', $_COOKIE['department'])
												->count();
												
				$completed_report_count = DB::table('report')
											->where('report.status_id', '=', HomeController::STATE_COMPLETED)
											->where('report.department_id', '=', $_COOKIE['department'])
											->count();
			}
		}else{
			$total_report_count = DB::table('report')
											->where('report.user_id', '=', $_COOKIE['user_id'])
											->count();
											
			$processing_report_count = DB::table('report')
											->whereIn('report.status_id', array(HomeController::STATE_SORTING, HomeController::STATE_SORTED, HomeController::STATE_PROCESSING))
											->where('report.user_id', '=', $_COOKIE['user_id'])
											->count();
											
			$completed_report_count = DB::table('report')
										->where('report.status_id', '=', HomeController::STATE_COMPLETED)
										->where('report.user_id', '=', $_COOKIE['user_id'])
										->count();
		}
		
		
		return View::make('page/index', array('page' => 'index',
											'reports' => $reports,
											'histories' => $histories,
											'total_report_count' => $total_report_count,
											'processing_report_count' => $processing_report_count,
											'completed_report_count' => $completed_report_count));
	}

}
